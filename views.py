from flask import render_template, flash, redirect
from app import app

@app.route('/', methods=('GET',) )
def index():
    return render_template("index.html",
                           title = "Home",
                          )

@app.route('/truc', methods=('GET',) )
def perdu():
    return "Tu t'es perdu", 404

@app.route('/listes', methods=('GET',) )
def listes():
    return "Page non encore implementee", 501

@app.route('/categories', methods=('GET',) )
def categories():
    return  "Page non encore implementee", 501

@app.route('/magasins', methods=('GET',) )
def magasins():
    return  "Page non encore implementee", 501

@app.route('/recettes', methods=('GET',) )
def recettes():
    return  "Page non encore implementee", 501

@app.route('/liste', methods=('GET',) )
def liste():
    return  render_template("liste.html",
                            title = "Liste",
                            )